#!/bin/bash

echo ""
echo "This script will add several directories to the current folder $PWD."
read -p "Confirm with \"y\" if you want to proceed: " -n 1 -r
echo    # (optional) move to a new line

if [[ $REPLY =~ ^[Yy]$ ]]
then
  # dune core modules
  for MOD in common geometry grid localfunctions istl; do
  git clone -b releases/2.8 https://gitlab.dune-project.org/core/dune-$MOD.git;
  done

  # dumux and dumux-reaktoro
  git clone -b tmp/avoid-privars-switch https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
  git clone https://git.iws.uni-stuttgart.de/dumux-appl/dumux-reaktoro.git

  # run dunecontrol
  ./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all

  echo ""
  echo "You need to personalize the hardcoded dependencies in dumux-reaktoro/test/2p2c/CMakeLists.txt."
  echo "Afterwards, change to dumux-reaktoro/build-cmake/test/2p2c, build and run by"
  echo "make test_2p2c_injection_tpfa && ./test_2p2c_injection_tpfa"
fi
