## DuMux - Reaktoro

This module couples the DuMux simulator to [Reaktoro](https://reaktoro.org/), a framework for modeling chemically reactive systems.
It is currently restricted to a prototype implementation which uses Reaktoro for a flash calculation inside an otherwise standard two-phase two-component model.

### Installation

1. Install Reaktoro using CMake by following [these instructions](https://reaktoro.org/installation/installation-using-cmake.html).

2. Add packages to the Conda Reaktoro environment:
```bash
conda install gfortran valgrind suitesparse -c conda-forge
```

3. You can use the script [installdumux-reaktoro.sh](installdumux-reaktoro.sh) to install all Dune and DuMux modules:
```bash
mkdir DumuxReaktoro
cd DumuxReaktoro
wget https://git.iws.uni-stuttgart.de/dumux-appl/dumux-reaktoro/-/blob/master/installdumux-reaktoro.sh
bash ./installdumux-reaktoro.sh
```

4. Personalize the hardcoded dependencies in your local [test/2p2c/CMakeLists.txt](test/2p2c/CMakeLists.txt).

5. Change to the test folder, build and run:
```bash
cd dumux-reaktoro/build-cmake/test/2p2c
make test_2p2c_injection_tpfa && ./test_2p2c_injection_tpfa
```

6. If necessary, goto 4.
