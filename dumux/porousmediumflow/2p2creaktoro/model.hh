// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_TWOPTWOCREAKTORO_MODEL_HH
#define DUMUX_TWOPTWOCREAKTORO_MODEL_HH

#include <dumux/porousmediumflow/2p/model.hh>
#include <dumux/porousmediumflow/2p2c/model.hh>
#include "volumevariables.hh"
#include "iofields.hh"

namespace Dumux
{

namespace Properties {

//////////////////////////////////////////////////////////////////
// Type tags
//////////////////////////////////////////////////////////////////
// Create new type tags
namespace TTag {
struct TwoPTwoCReaktoro { using InheritsFrom = std::tuple<TwoPTwoC>; };
} // end namespace TTag

//////////////////////////////////////////////////////////////////
// Property values
//////////////////////////////////////////////////////////////////

template<int nComp, bool useMol, bool setMoleFractionForFP, TwoPFormulation formulation, int repCompEqIdx = nComp>
struct TwoPTwoCReaktoroModelTraits : public TwoPNCModelTraits<nComp, useMol, setMoleFractionForFP, formulation, repCompEqIdx>
{
    static constexpr int numEq() { return nComp + 1; }
};

/*!
 * \brief Set the model traits property.
 */
template<class TypeTag>
struct BaseModelTraits<TypeTag, TTag::TwoPTwoCReaktoro>
{
private:
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
public:
    using type = TwoPTwoCReaktoroModelTraits<FluidSystem::numComponents,
                                             getPropValue<TypeTag, Properties::UseMoles>(),
                                             /*setMFracForFirstPhase=*/true,
                                             getPropValue<TypeTag, Properties::Formulation>(),
                                             getPropValue<TypeTag, Properties::ReplaceCompEqIdx>()>;
};
template<class TypeTag>
struct ModelTraits<TypeTag, TTag::TwoPTwoCReaktoro>
{ using type = GetPropType<TypeTag, Properties::BaseModelTraits>; };

template<class TypeTag>
struct PrimaryVariables<TypeTag, TTag::TwoPTwoCReaktoro>
{
    using type = Dune::FieldVector<GetPropType<TypeTag, Properties::Scalar>,
                                   GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
};

//! Use the 2p2c VolumeVariables
template<class TypeTag>
struct VolumeVariables<TypeTag, TTag::TwoPTwoCReaktoro>
{
private:
    using PV = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using FSY = GetPropType<TypeTag, Properties::FluidSystem>;
    using FST = GetPropType<TypeTag, Properties::FluidState>;
    using SSY = GetPropType<TypeTag, Properties::SolidSystem>;
    using SST = GetPropType<TypeTag, Properties::SolidState>;
    using PT = typename GetPropType<TypeTag, Properties::SpatialParams>::PermeabilityType;
    using MT = GetPropType<TypeTag, Properties::ModelTraits>;
    using DM = typename GetPropType<TypeTag, Properties::GridGeometry>::DiscretizationMethod;
    static constexpr bool enableIS = getPropValue<TypeTag, Properties::EnableBoxInterfaceSolver>();
    static_assert(FSY::numComponents == 2, "Only fluid systems with 2 components are supported by the 2p2c model!");
    static_assert(FSY::numPhases == 2, "Only fluid systems with 2 phases are supported by the 2p2c model!");
    // class used for scv-wise reconstruction of nonwetting phase saturations
    using SR = TwoPScvSaturationReconstruction<DM, enableIS>;
    using BaseTraits = TwoPVolumeVariablesTraits<PV, FSY, FST, SSY, SST, PT, MT, SR>;

    using DT = GetPropType<TypeTag, Properties::MolecularDiffusionType>;
    using EDM = GetPropType<TypeTag, Properties::EffectiveDiffusivityModel>;
    template<class BaseTraits, class DT, class EDM>
    struct NCTraits : public BaseTraits
    {
        using DiffusionType = DT;
        using EffectiveDiffusivityModel = EDM;
    };
public:
    using type = TwoPTwoCReaktoroVolumeVariables<NCTraits<BaseTraits, DT, EDM>>;
};

//! Set the vtk output fields specific to this model
template<class TypeTag>
struct IOFields<TypeTag, TTag::TwoPTwoCReaktoro> { using type = TwoPTwoCReaktoroIOFields; };

} // end namespace Properties
} // end namespace Dumux

#endif
