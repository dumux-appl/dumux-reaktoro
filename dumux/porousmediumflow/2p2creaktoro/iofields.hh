// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPTwoCReaktoroModel
 * \brief Adds I/O fields specific to the 2p2c-Reaktoro model.
 */

#ifndef DUMUX_TWOPTWOCREAKTORO_IO_FIELDS_HH
#define DUMUX_TWOPTWOCREAKTORO_IO_FIELDS_HH

#include <dumux/porousmediumflow/2pnc/iofields.hh>

namespace Dumux
{

/*!
 * \ingroup TwoPTwoCReaktoroModel
 * \brief Adds I/O fields specific to the 2p2c-Reaktoro model.
 */
class TwoPTwoCReaktoroIOFields : public TwoPNCIOFields
{
public:
    template <class OutputModule>
    static void initOutputModule(OutputModule& out)
    {
        using VolumeVariables = typename OutputModule::VolumeVariables;
        using FluidSystem = typename VolumeVariables::FluidSystem;

        // use default fields from the 2p model
        TwoPIOFields::initOutputModule(out);

        // output additional to TwoP output:
        for (int phaseIdx = 0; phaseIdx < VolumeVariables::numFluidPhases(); ++phaseIdx)
        {
            for (int compIdx = 0; compIdx < VolumeVariables::numFluidComponents(); ++compIdx)
            {
                out.addVolumeVariable([phaseIdx,compIdx](const auto& v){ return v.moleFraction(phaseIdx,compIdx); },
                                      IOName::moleFraction<FluidSystem>(phaseIdx, compIdx));
                if (VolumeVariables::numFluidComponents() < 3)
                    out.addVolumeVariable([phaseIdx,compIdx](const auto& v){ return v.massFraction(phaseIdx,compIdx); },
                                          IOName::massFraction<FluidSystem>(phaseIdx, compIdx));
            }

            out.addVolumeVariable([phaseIdx](const auto& v){ return v.molarDensity(phaseIdx); },
                                    IOName::molarDensity<FluidSystem>(phaseIdx));
        }
    }
};

} // end namespace Dumux

#endif
