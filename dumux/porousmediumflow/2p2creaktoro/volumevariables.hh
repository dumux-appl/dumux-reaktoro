// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Contains the quantities which are constant within a
 *        finite volume in the TwoPTwoCReaktoro model.
 */
#ifndef DUMUX_TWOPTWOCREAKTORO_VOLUME_VARIABLES_HH
#define DUMUX_TWOPTWOCREAKTORO_VOLUME_VARIABLES_HH

#include <dumux/common/deprecated.hh>
#include <dumux/material/solidstates/updatesolidvolumefractions.hh>
#include <dumux/porousmediumflow/2p2c/volumevariables.hh>
#include <Reaktoro/Reaktoro.hpp>

namespace Dumux
{
template <class Traits>
class TwoPTwoCReaktoroVolumeVariables
: public TwoPTwoCVolumeVariablesBase<Traits, TwoPTwoCReaktoroVolumeVariables<Traits>>
{
    using ParentType = TwoPTwoCVolumeVariablesBase<Traits, TwoPTwoCReaktoroVolumeVariables<Traits>>;
    using EnergyVolVars = EnergyVolumeVariables<Traits, TwoPTwoCReaktoroVolumeVariables<Traits> >;

    using Scalar = typename Traits::PrimaryVariables::value_type;
    using ModelTraits = typename Traits::ModelTraits;

    static constexpr int numFluidComps = ParentType::numFluidComponents();

    // component indices
    enum
    {
        comp0Idx = Traits::FluidSystem::comp0Idx,
        comp1Idx = Traits::FluidSystem::comp1Idx,
        phase0Idx = Traits::FluidSystem::phase0Idx,
        phase1Idx = Traits::FluidSystem::phase1Idx
    };

    // primary variable indices
    enum
    {
        switchIdx = ModelTraits::Indices::switchIdx,
        pressureIdx = 2 //ModelTraits::Indices::pressureIdx
    };

    // formulations
    static constexpr auto formulation = ModelTraits::priVarFormulation();

public:
    //! The type of the object returned by the fluidState() method
    using FluidState = typename Traits::FluidState;
    //! The fluid system used here
    using FluidSystem = typename Traits::FluidSystem;
    //! Export type of solid state
    using SolidState = typename Traits::SolidState;
    //! Export type of solid system
    using SolidSystem = typename Traits::SolidSystem;
    //! Export the primary variable switch
    using PrimaryVariableSwitch = TwoPNCPrimaryVariableSwitch;

    //! Return whether moles or masses are balanced
    static constexpr bool useMoles() { return ModelTraits::useMoles(); }
    //! Return the two-phase formulation used here
    static constexpr TwoPFormulation priVarFormulation() { return formulation; }

    /*!
     * \brief Sets complete fluid state.
     *
     * \param elemSol A vector containing all primary variables connected to the element
     * \param problem The object specifying the problem which ought to
     *                be simulated
     * \param element An element which contains part of the control volume
     * \param scv The sub-control volume
     * \param fluidState A container with the current (physical) state of the fluid
     * \param solidState A container with the current (physical) state of the solid
     *
     * Set temperature, saturations, capillary pressures, viscosities, densities and enthalpies.
     */
    template<class ElemSol, class Problem, class Element, class Scv>
    void completeFluidState(const ElemSol& elemSol,
                            const Problem& problem,
                            const Element& element,
                            const Scv& scv,
                            FluidState& fluidState,
                            SolidState& solidState)
    {
        auto T = Deprecated::temperature(problem, element, scv, elemSol);
        for(int phaseIdx=0; phaseIdx < FluidSystem::numPhases; ++phaseIdx)
        {
            fluidState.setTemperature(phaseIdx, T);
        }
        solidState.setTemperature(T);

        const auto& priVars = elemSol[scv.localDofIndex()];

        static auto database = Reaktoro::SupcrtDatabase("supcrtbl");

        static auto liquids = Reaktoro::AqueousPhase("H2O(aq) N2(aq)");
        static auto gases = Reaktoro::GaseousPhase("N2(g) H2O(g)");

        static auto reaktoroSystem = Reaktoro::ChemicalSystem(database, liquids, gases);
        static auto solver = Reaktoro::EquilibriumSolver(reaktoroSystem);

        auto reaktoroState = Reaktoro::ChemicalState(reaktoroSystem);

        try {
            reaktoroState.temperature(fluidState.temperature(), "kelvin");
            reaktoroState.pressure(priVars[pressureIdx], "Pa");
            updateSolidVolumeFractions(elemSol, problem, element, scv, solidState, numFluidComps);
            Scalar poreVolume = scv.volume()*solidState.porosity();
            reaktoroState.set("H2O(aq)", std::max(priVars[comp0Idx]*poreVolume, 0.0), "kg");
            reaktoroState.set("N2(g)",  std::max(priVars[comp1Idx]*poreVolume, 0.0), "kg");

            // std::cout << "BEFORE" << std::endl;
            // std::cout << reaktoroState << std::endl;
            solver.solve(reaktoroState);
            // std::cout << "AFTER" << std::endl;
            // std::cout << reaktoroState << std::endl;
        }
        catch (std::exception& e) {
            std::cout << "\n\nCaught Reaktoro exception at vertex "
            << scv.dofPosition() << ": " << std::endl;
            std::cout << reaktoroState << std::endl;
            std::cout << e.what() << std::endl;
            exit(1);
        }

        const auto& reaktoroProps = reaktoroState.props();
        Scalar sw = reaktoroProps.phaseProps(0).volume()/reaktoroProps.volume();

        const auto wPhaseIdx = problem.spatialParams().template wettingPhase<FluidSystem>(element, scv, elemSol);
        fluidState.setWettingPhase(wPhaseIdx);
        const auto nPhaseIdx = 1 - wPhaseIdx;
        fluidState.setSaturation(wPhaseIdx, sw);
        fluidState.setSaturation(nPhaseIdx, 1.0 - sw);

        const auto fluidMatrixInteraction = problem.spatialParams().fluidMatrixInteraction(element, scv, elemSol);
        Scalar pc = fluidMatrixInteraction.pc(sw);

        if (formulation == TwoPFormulation::p0s1) {
            fluidState.setPressure(wPhaseIdx, priVars[pressureIdx]);
            fluidState.setPressure(nPhaseIdx, priVars[pressureIdx] + pc);
        }
        else if (formulation == TwoPFormulation::p1s0) {
            fluidState.setPressure(nPhaseIdx, priVars[pressureIdx]);
            fluidState.setPressure(wPhaseIdx, priVars[pressureIdx] - pc);
        }
        else
            DUNE_THROW(Dune::InvalidStateException, "Formulation is invalid.");

        const auto& liquidMoleFractions = reaktoroProps.phaseProps(0).speciesMoleFractions();
        fluidState.setMoleFraction(wPhaseIdx, comp0Idx, liquidMoleFractions.data()[0][0]);
        fluidState.setMoleFraction(wPhaseIdx, comp1Idx, liquidMoleFractions.data()[1][0]);

        const auto& gaseousMoleFractions = reaktoroProps.phaseProps(1).speciesMoleFractions();
        fluidState.setMoleFraction(nPhaseIdx, comp0Idx, gaseousMoleFractions.data()[1][0]);
        fluidState.setMoleFraction(nPhaseIdx, comp1Idx, gaseousMoleFractions.data()[0][0]);

        typename FluidSystem::ParameterCache paramCache;
        paramCache.updateAll(fluidState);

        for (int phaseIdx = 0; phaseIdx < ModelTraits::numFluidPhases(); ++phaseIdx)
        {
            paramCache.updateComposition(fluidState, phaseIdx);
            const Scalar rho = FluidSystem::density(fluidState, paramCache, phaseIdx);
            fluidState.setDensity(phaseIdx, rho);
            Scalar rhoMolar = FluidSystem::molarDensity(fluidState, paramCache, phaseIdx);
            fluidState.setMolarDensity(phaseIdx, rhoMolar);

            // compute and set the enthalpy
            const Scalar mu = FluidSystem::viscosity(fluidState, paramCache, phaseIdx);
            fluidState.setViscosity(phaseIdx,mu);
            Scalar h = EnergyVolVars::enthalpy(fluidState, paramCache, phaseIdx);
            fluidState.setEnthalpy(phaseIdx, h);
        }
   }

};


} // end namespace

#endif
